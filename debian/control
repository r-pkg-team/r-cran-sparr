Source: r-cran-sparr
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-sparr
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-sparr.git
Homepage: https://cran.r-project.org/package=sparr
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-spatstat (>= 2.3-0),
               r-cran-spatstat.utils,
               r-cran-spatstat.geom,
               r-cran-spatstat.explore,
               r-cran-spatstat.random,
               r-cran-doparallel,
               r-cran-foreach,
               r-cran-misc3d
Testsuite: autopkgtest-pkg-r

Package: r-cran-sparr
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R spatial and spatiotemporal relative risk
 Provides functions to estimate kernel-smoothed spatial and spatio-
 temporal densities and relative risk functions, and perform subsequent
 inference. Methodological details can be found in the accompanying
 tutorial: Davies et al. (2018) <DOI:10.1002/sim.7577>.
